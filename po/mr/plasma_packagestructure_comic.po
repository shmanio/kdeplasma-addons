# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-25 00:19+0000\n"
"PO-Revision-Date: 2013-03-20 17:37+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: comic_package.cpp:20
#, kde-format
msgid "Images"
msgstr "प्रतिमा"

#: comic_package.cpp:25
#, kde-format
msgid "Executable Scripts"
msgstr "एक्जीक्यूटेबल स्क्रिप्ट्स"

#: comic_package.cpp:29
#, kde-format
msgid "Main Script File"
msgstr "मुख्य स्क्रिप्ट फाईल"
