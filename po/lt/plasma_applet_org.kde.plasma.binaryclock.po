# Lithuanian translations for plasma_applet_binaryclock package.
# This file is distributed under the same license as the plasma_applet_binaryclock package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2008.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Ališauskas <liudas@akmc.lt>, 2012, 2013.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_binaryclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 02:25+0200\n"
"PO-Revision-Date: 2019-06-01 19:22+0000\n"
"Last-Translator: Moo\n"
"Language-Team: lt <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 2.0.6\n"

#: package/contents/config/config.qml:17
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Išvaizda"

#: package/contents/ui/configGeneral.qml:35
#, kde-format
msgid "Display:"
msgstr "Rodyti:"

#: package/contents/ui/configGeneral.qml:36
#: package/contents/ui/configGeneral.qml:90
#, kde-format
msgctxt "@option:check"
msgid "Grid"
msgstr "Tinklelis"

#: package/contents/ui/configGeneral.qml:41
#: package/contents/ui/configGeneral.qml:77
#, kde-format
msgctxt "@option:check"
msgid "Inactive LEDs"
msgstr "Neaktyvūs šviesos diodai"

#: package/contents/ui/configGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Seconds"
msgstr "Sekundės"

#: package/contents/ui/configGeneral.qml:51
#, kde-format
msgctxt "@option:check"
msgid "In BCD format (decimal)"
msgstr "BCD formatu (dešimtainis)"

#: package/contents/ui/configGeneral.qml:60
#, kde-format
msgid "Use custom color for:"
msgstr "Naudoti tinkintą spalvą, skirtą:"

#: package/contents/ui/configGeneral.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Active LEDs"
msgstr "Aktyvūs šviesos diodai"

#, fuzzy
#~| msgid "Show the inactive LEDs"
#~ msgctxt "@option:check"
#~ msgid "Show inactive LEDs"
#~ msgstr "Rodyti neaktyvus LED"

#, fuzzy
#~| msgid "Use custom color for active LEDs:"
#~ msgctxt "@option:check"
#~ msgid "Use custom color for active LEDs"
#~ msgstr "Naudoti savitą spalvą aktyviems LED'ams:"

#, fuzzy
#~| msgid "Use custom color for inactive LEDs:"
#~ msgctxt "@option:check"
#~ msgid "Use custom color for inactive LEDs"
#~ msgstr "Naudoti parinktą spalvą neaktyviems LED'ams:"

#, fuzzy
#~| msgid "Appearance"
#~ msgctxt "@title:group"
#~ msgid "Appearance"
#~ msgstr "Išvaizda"

#~ msgid "Check this if you want to see the inactive LEDs."
#~ msgstr "Pažymėkite, jei norite matyti neaktyvius LED."

#~ msgid "Show"
#~ msgstr "Rodyti"

#~ msgid "Use theme color"
#~ msgstr "Naudoti temos spalvą"

#~ msgid "Show the grid"
#~ msgstr "Rodyti tinklelį"

#~ msgid "Check this if you want to see a grid around leds."
#~ msgstr "Pažymėkite, jei norite matyti tinklelį aplink LED."

#~ msgid "Use custom grid color:"
#~ msgstr "Naudoti savitą tinklelio spalvą:"

#~ msgid "Information"
#~ msgstr "Informacija"

#~ msgid "Show the seconds LEDs"
#~ msgstr "Rodyti sekudes LED'e"

#~ msgid ""
#~ "Check this if you want to display seconds LEDs in order to see the "
#~ "seconds."
#~ msgstr ""
#~ "Pažymėkite, jei norite, kad būtų rodomi sekundžių LED, kad matyti "
#~ "sekundes."
