# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2010, 2015, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_datetime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-31 00:47+0000\n"
"PO-Revision-Date: 2021-12-27 17:10+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: datetimerunner.cpp:20
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "date"
msgstr "data"

#: datetimerunner.cpp:21
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "time"
msgstr "ora"

#: datetimerunner.cpp:28
#, kde-format
msgid "Displays the current date"
msgstr "Visualizza la data attuale"

#: datetimerunner.cpp:29
#, kde-format
msgid "Displays the current time"
msgstr "Visualizza l'ora attuale"

#: datetimerunner.cpp:30 datetimerunner.cpp:32
#, kde-format
msgctxt "The <> and space are part of the example query"
msgid " <timezone>"
msgstr " <fusoorario>"

#: datetimerunner.cpp:31
#, kde-format
msgid "Displays the current date in a given timezone"
msgstr "Visualizza la data attuale in un dato fuso orario"

#: datetimerunner.cpp:33
#, kde-format
msgid "Displays the current time in a given timezone"
msgstr "Visualizza l'ora attuale in un dato fuso orario"

#: datetimerunner.cpp:46
#, kde-format
msgid "Today's date is %1"
msgstr "La data odierna è %1"

#: datetimerunner.cpp:60
#, kde-format
msgid "Current time is %1"
msgstr "L'ora attuale è %1"

#: datetimerunner.cpp:77
#, kde-format
msgctxt "time zone difference"
msgid "later"
msgstr ""

#: datetimerunner.cpp:78
#, kde-format
msgctxt "time zone difference"
msgid "earlier"
msgstr ""

#: datetimerunner.cpp:79
#, kde-format
msgctxt "no time zone difference"
msgid "no time difference"
msgstr ""

#~ msgid "The date in %1 is %2"
#~ msgstr "La data in %1 è %2"

#~ msgid "The current time in %1 is %2"
#~ msgstr "L'ora corrente in %1 è %2"
