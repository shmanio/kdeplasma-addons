# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Sergiu Bivol <sergiu@cip.md>, 2020, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-28 00:51+0000\n"
"PO-Revision-Date: 2022-08-14 16:49+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/config.qml:48
#, kde-format
msgctxt "@label:listbox"
msgid "Provider:"
msgstr "Furnizor:"

#: package/contents/ui/config.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Update when using metered network connection"
msgstr "Actualizează la folosirea unei conexiuni de rețea contorizate"

#: package/contents/ui/config.qml:76
#, kde-format
msgctxt "@label:listbox"
msgid "Category:"
msgstr "Categorie:"

#: package/contents/ui/config.qml:80
#, kde-format
msgctxt "@item:inlistbox"
msgid "All"
msgstr "Toate"

#: package/contents/ui/config.qml:84
#, kde-format
msgctxt "@item:inlistbox"
msgid "1080p"
msgstr "1080p"

#: package/contents/ui/config.qml:88
#, kde-format
msgctxt "@item:inlistbox"
msgid "4K"
msgstr "4K"

#: package/contents/ui/config.qml:92
#, kde-format
msgctxt "@item:inlistbox"
msgid "Ultra Wide"
msgstr "Ultra-lat"

#: package/contents/ui/config.qml:96
#, kde-format
msgctxt "@item:inlistbox"
msgid "Background"
msgstr "Fundal"

#: package/contents/ui/config.qml:100
#, kde-format
msgctxt "@item:inlistbox"
msgid "Lock Screen"
msgstr "Ecran de blocare"

#: package/contents/ui/config.qml:104
#, kde-format
msgctxt "@item:inlistbox"
msgid "Nature"
msgstr "Natură"

#: package/contents/ui/config.qml:108
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tumblr"
msgstr "Tumblr"

#: package/contents/ui/config.qml:112
#, kde-format
msgctxt "@item:inlistbox"
msgid "Black"
msgstr "Negru"

#: package/contents/ui/config.qml:116
#, kde-format
msgctxt "@item:inlistbox"
msgid "Flower"
msgstr "Floare"

#: package/contents/ui/config.qml:120
#, kde-format
msgctxt "@item:inlistbox"
msgid "Funny"
msgstr "Nostim"

#: package/contents/ui/config.qml:124
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cute"
msgstr "Drăguț"

#: package/contents/ui/config.qml:128
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cool"
msgstr "Fain"

#: package/contents/ui/config.qml:132
#, kde-format
msgctxt "@item:inlistbox"
msgid "Fall"
msgstr "Toamna"

#: package/contents/ui/config.qml:136
#, kde-format
msgctxt "@item:inlistbox"
msgid "Love"
msgstr "Dragoste"

#: package/contents/ui/config.qml:140
#, kde-format
msgctxt "@item:inlistbox"
msgid "Design"
msgstr "Proiectare"

#: package/contents/ui/config.qml:144
#, kde-format
msgctxt "@item:inlistbox"
msgid "Christmas"
msgstr "Crăciun"

#: package/contents/ui/config.qml:148
#, kde-format
msgctxt "@item:inlistbox"
msgid "Travel"
msgstr "Călătorie"

#: package/contents/ui/config.qml:152
#, kde-format
msgctxt "@item:inlistbox"
msgid "Beach"
msgstr "Plajă"

#: package/contents/ui/config.qml:156
#, kde-format
msgctxt "@item:inlistbox"
msgid "Car"
msgstr "Mașină"

#: package/contents/ui/config.qml:160
#, kde-format
msgctxt "@item:inlistbox"
msgid "Sports"
msgstr "Sporturi"

#: package/contents/ui/config.qml:164
#, kde-format
msgctxt "@item:inlistbox"
msgid "Animal"
msgstr "Animal"

#: package/contents/ui/config.qml:168
#, kde-format
msgctxt "@item:inlistbox"
msgid "People"
msgstr "Oameni"

#: package/contents/ui/config.qml:172
#, kde-format
msgctxt "@item:inlistbox"
msgid "Music"
msgstr "Muzică"

#: package/contents/ui/config.qml:176
#, kde-format
msgctxt "@item:inlistbox"
msgid "Summer"
msgstr "Vara"

#: package/contents/ui/config.qml:180
#, kde-format
msgctxt "@item:inlistbox"
msgid "Galaxy"
msgstr "Galaxie"

#: package/contents/ui/config.qml:184
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tabliss"
msgstr "Tabliss"

#: package/contents/ui/config.qml:214
#, kde-format
msgctxt "@label:listbox"
msgid "Positioning:"
msgstr "Poziționare:"

#: package/contents/ui/config.qml:217
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled and Cropped"
msgstr "Scalat și decupat"

#: package/contents/ui/config.qml:221
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled"
msgstr "Scalat"

#: package/contents/ui/config.qml:225
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled, Keep Proportions"
msgstr "Scalat, păstrare proporții"

#: package/contents/ui/config.qml:229
#, kde-format
msgctxt "@item:inlistbox"
msgid "Centered"
msgstr "Centrat"

#: package/contents/ui/config.qml:233
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tiled"
msgstr "Dale"

#: package/contents/ui/config.qml:254
#, kde-format
msgctxt "@label:chooser"
msgid "Background color:"
msgstr "Culoarea de fundal:"

#: package/contents/ui/config.qml:255
#, kde-format
msgctxt "@title:window"
msgid "Select Background Color"
msgstr "Alege culoarea de fundal"

#: package/contents/ui/config.qml:266
#, kde-format
msgctxt "@label"
msgid "Today's picture:"
msgstr "Imaginea zilei:"

#: package/contents/ui/config.qml:283
#, kde-format
msgctxt "@label"
msgid "Title:"
msgstr "Titlu:"

#: package/contents/ui/config.qml:303
#, kde-format
msgctxt "@label"
msgid "Author:"
msgstr "Autor:"

#: package/contents/ui/config.qml:329
#, kde-format
msgctxt "@action:button"
msgid "Open Containing Folder"
msgstr "Deschide dosarul conținător"

#: package/contents/ui/config.qml:333
#, kde-format
msgctxt "@info:whatsthis for a button"
msgid "Open the destination folder where the wallpaper image was saved."
msgstr "Deschide dosarul-destinație în care a fost salvată imaginea tapetului."

#: package/contents/ui/WallpaperDelegate.qml:142
#, kde-format
msgctxt "@info:whatsthis"
msgid "Today's picture"
msgstr "Imaginea zilei"

#: package/contents/ui/WallpaperDelegate.qml:143
#, kde-format
msgctxt "@info:whatsthis"
msgid "Loading"
msgstr "Se încarcă"

#: package/contents/ui/WallpaperDelegate.qml:144
#, kde-format
msgctxt "@info:whatsthis"
msgid "Unavailable"
msgstr "Indisponibil"

#: package/contents/ui/WallpaperDelegate.qml:145
#, kde-format
msgctxt "@info:whatsthis for an image %1 title %2 author"
msgid "%1 Author: %2. Right-click on the image to see more actions."
msgstr ""
"%1 Autor: %2. Faceți clic-dreapta pe imagine pentru acțiuni suplimentare."

#: package/contents/ui/WallpaperDelegate.qml:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "The wallpaper is being fetched from the Internet."
msgstr "Tapetul se descarcă din Internet."

#: package/contents/ui/WallpaperDelegate.qml:147
#, kde-format
msgctxt "@info:whatsthis"
msgid "Failed to fetch the wallpaper from the Internet."
msgstr "Descărcarea tapetului din Internet a eșuat."

#: package/contents/ui/WallpaperPreview.qml:50
#, kde-format
msgctxt "@action:inmenu wallpaper preview menu"
msgid "Save Image as…"
msgstr "Salvează imaginea ca…"

#: package/contents/ui/WallpaperPreview.qml:53
#, kde-format
msgctxt "@info:whatsthis for a button and a menu item"
msgid "Save today's picture to local disk"
msgstr "Salvează imaginea zilei pe discul local"

#: package/contents/ui/WallpaperPreview.qml:59
#, kde-format
msgctxt ""
"@action:inmenu wallpaper preview menu, will open the website of the wallpaper"
msgid "Open Link in Browser…"
msgstr "Deschide legătura în navigator…"

#: package/contents/ui/WallpaperPreview.qml:62
#, kde-format
msgctxt "@info:whatsthis for a menu item"
msgid "Open the website of today's picture in the default browser"
msgstr "Deschide saitul imaginii zilei în navigatorul implicit"

#: plugins/potdbackend.cpp:221
#, kde-format
msgctxt "@title:window"
msgid "Save Today's Picture"
msgstr "Salvează imaginea zilei"

#: plugins/potdbackend.cpp:223
#, kde-format
msgctxt "@label:listbox Template for file dialog"
msgid "JPEG image (*.jpeg *.jpg *.jpe)"
msgstr "Imagine JPEG (*.jpeg *.jpg *.jpe)"

#: plugins/potdbackend.cpp:240
#, kde-format
msgctxt "@info:status after a save action"
msgid "The image was not saved."
msgstr "Imaginea nu a fost salvată."

#: plugins/potdbackend.cpp:246
#, kde-format
msgctxt "@info:status after a save action %1 file path %2 basename"
msgid "The image was saved as <a href=\"%1\">%2</a>"
msgstr "Imaginea a fost salvată ca <a href=\"%1\">%2</a>"
