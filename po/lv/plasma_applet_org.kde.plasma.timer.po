# translation of plasma_applet_timer.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008.
# Maris Nartiss <maris.kde@gmail.com>, 2008.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_timer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-01 00:49+0000\n"
"PO-Revision-Date: 2009-06-03 20:50+0300\n"
"Last-Translator: Viesturs Zariņš <viesturs.zarins@mii.lu.lv>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: package/contents/config/config.qml:13
#, fuzzy, kde-format
#| msgid "Appearance"
msgctxt "@title"
msgid "Appearance"
msgstr "Izskats"

#: package/contents/config/config.qml:19
#, fuzzy, kde-format
#| msgid "Predefined Timers"
msgctxt "@title"
msgid "Predefined Timers"
msgstr "Gatavi taimeri"

#: package/contents/config/config.qml:24
#, kde-format
msgctxt "@title"
msgid "Advanced"
msgstr ""

#: package/contents/ui/CompactRepresentation.qml:133
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Pause Timer"
msgstr "Taimeris"

#: package/contents/ui/CompactRepresentation.qml:133
#, fuzzy, kde-format
#| msgid "Timer"
msgctxt "@action:button"
msgid "Start Timer"
msgstr "Taimeris"

#: package/contents/ui/CompactRepresentation.qml:189
#: package/contents/ui/CompactRepresentation.qml:206
#, kde-format
msgctxt "remaining time"
msgid "%1s"
msgid_plural "%1s"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: package/contents/ui/configAdvanced.qml:22
#, kde-format
msgctxt "@title:label"
msgid "After timer completes:"
msgstr ""

#: package/contents/ui/configAdvanced.qml:26
#, fuzzy, kde-format
#| msgid "Run a command:"
msgctxt "@option:check"
msgid "Execute command:"
msgstr "Darbināt komandu:"

#: package/contents/ui/configAppearance.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Display:"
msgstr ""

#: package/contents/ui/configAppearance.qml:33
#, fuzzy, kde-format
#| msgid "Show title:"
msgctxt "@option:check"
msgid "Show title:"
msgstr "Rādīt virsrakstu:"

#: package/contents/ui/configAppearance.qml:50
#, fuzzy, kde-format
#| msgid "Hide seconds"
msgctxt "@option:check"
msgid "Show seconds"
msgstr "Slēpt sekundes"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgctxt "@title:label"
msgid "Notifications:"
msgstr ""

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Show notification text:"
msgstr ""

#: package/contents/ui/configTimes.qml:76
#, kde-format
msgid ""
"If you add predefined timers here, they will appear in plasmoid context menu."
msgstr ""

#: package/contents/ui/configTimes.qml:83
#, kde-format
msgid "Add"
msgstr ""

#: package/contents/ui/configTimes.qml:120
#, kde-format
msgid "Scroll over digits to change time"
msgstr ""

#: package/contents/ui/configTimes.qml:126
#, kde-format
msgid "Apply"
msgstr ""

#: package/contents/ui/configTimes.qml:134
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/configTimes.qml:143
#, kde-format
msgid "Edit"
msgstr ""

#: package/contents/ui/configTimes.qml:152
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:56
#, kde-format
msgid "%1 is running"
msgstr ""

#: package/contents/ui/main.qml:58
#, kde-format
msgid "%1 not running"
msgstr ""

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Remaining time left: %1 second"
msgid_plural "Remaining time left: %1 seconds"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: package/contents/ui/main.qml:62
#, kde-format
msgid ""
"Use mouse wheel to change digits or choose from predefined timers in the "
"context menu"
msgstr ""

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Timer"
msgstr "Taimeris"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Timer finished"
msgstr ""

#: package/contents/ui/main.qml:137
#, fuzzy, kde-format
#| msgid "Start"
msgctxt "@action"
msgid "&Start"
msgstr "Palaist"

#: package/contents/ui/main.qml:138
#, kde-format
msgctxt "@action"
msgid "S&top"
msgstr ""

#: package/contents/ui/main.qml:139
#, fuzzy, kde-format
#| msgid "Reset"
msgctxt "@action"
msgid "&Reset"
msgstr "Nomest"

#, fuzzy
#~| msgid "Run a command:"
#~ msgctxt "@title:group"
#~ msgid "Run Command"
#~ msgstr "Darbināt komandu:"

#, fuzzy
#~| msgid "Command:"
#~ msgctxt "@label:textbox"
#~ msgid "Command:"
#~ msgstr "Komanda:"

#~ msgctxt "separator of hours:minutes:seconds in timer strings"
#~ msgid ":"
#~ msgstr ":"

#~ msgid "Timer Configuration"
#~ msgstr "Taimera konfigurācija"

#~ msgid "Stop"
#~ msgstr "Apturēt"

#~ msgid "Timer Timeout"
#~ msgstr "Taimera noildze"

#~ msgid "General"
#~ msgstr "Pamata"

#~ msgid "Actions on Timeout"
#~ msgstr "Darbības pēc laika iztecēšanas"

#~ msgid "Show a message:"
#~ msgstr "Parādīt paziņojumu:"

#~ msgid "Message:"
#~ msgstr "Paziņoums:"
