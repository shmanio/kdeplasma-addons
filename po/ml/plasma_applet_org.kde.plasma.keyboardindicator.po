# Malayalam translations for kdeplasma-addons package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2018-11-09 04:04+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr ""

#: contents/ui/configAppearance.qml:34
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr ""

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr ""

#: contents/ui/configAppearance.qml:44
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr ""

#: contents/ui/main.qml:30
#, kde-format
msgid "Caps Lock activated\n"
msgstr ""

#: contents/ui/main.qml:31
#, kde-format
msgid "Num Lock activated\n"
msgstr ""

#: contents/ui/main.qml:111
#, kde-format
msgid "No lock keys activated"
msgstr ""
